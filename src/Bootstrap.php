<?php




    use php\lib\String;
    use php\util\Regex;
    use php\webserver\WebServer;
    use Sandwich\Http\Routing\Route;

    class Bootstrap
    {
        static function run()
        {
//            $router = new Route();
//
//            $router->map( 'GET, post, Put,file', '/[action]/id[i:id]', function() {
//                echo 321;
//            }, 'profile');
//
//            $router->map( 'GET', '/[script].js', function() {
//                echo 123;
//            }, 'home');



//            Route::dispatch('/user/id123', 'GET', null);
//            Route::dispatch('/home/id321', 'GET', null);

//            $match = $router->match('/profile/id12', 'GET');
//
//            print_r($match);
//
//            $match = $router->match('/test.js', 'GET');
//
//            print_r($match);
//
//            $match = $router->match('/testajs', 'GET');
//
//            print_r($match);

//            exit;

//
//            $url = [
//                '/',
//                '/scripts/test.js',
//                '/styles/common.css',
//                '/script.php',
//                '/folder1/folder2/folder3',
//                '/miner',
//            ];
//
//            $matchTypes = [
//                'i'  => '[0-9]++',
//                'a'  => '[0-9A-Za-z]++',
//                'h'  => '[0-9A-Fa-f]++',
//                '*'  => '.+?',
//                '**' => '.++',
//                ''   => '[^/\.]++'
//            ];
//
//            $needle = $url[1];
//
//            $namedPattern = '/scripts/[script].js';
//
//            $params = [];
//            $pattern = Regex::of('(\[(([^.]+):)?([a-z_][a-z0-9]+)\])', Regex::CASE_INSENSITIVE)->with($namedPattern)->replaceWithCallback(function(Regex $self) use(&$params, $matchTypes) {
//                $patternId = $self->group(3);
//                if($patternId === null)
//                    $patternId = '';
//                $paramName = $self->group(4);
//
//                $params[] = $paramName;
//
//                var_dump('PATTERN_ID: '.$patternId);
//                echo '$matchTypes[$patternId]: ';
//                var_dump($matchTypes[$patternId]);
//
//                return '('.$matchTypes[$patternId].')';
//            });
//
//            echo "pattern:";
//            var_dump($pattern);
//
//            echo "params:";
//            var_dump($params);
//
//            $regex = Regex::of($pattern, Regex::CASE_INSENSITIVE)->with($needle);
//            if($regex->matches()) {
//                echo  "Matches success\r\n";
//                $count = $regex->getGroupCount();
//                var_dump($regex->group(1));
//                echo "Founded parameters ({$count})\r\n";
//            } else {
//                echo  "Matches failed\r\n";
//            }
//            exit;
//            exit;
//
//            function matchUrl($url, $routerPattern)
//            {
////                $re = Regex::of($routerPattern, Regex::CASE_INSENSITIVE);
////                $re->with($url);
////                $re->lookingAt();
////                print_r($re->group(1));
////                echo $re->getGroupCount();
////                print_r($re->group(1));
//
////                $regex = Regex::of($routerPattern, Regex::CASE_INSENSITIVE);
//
////                foreach ($regex->with($url) as $i => $number) {
////                    var_dump($number);
////                }
////                print_r( Regex::split($routerPattern, $url) );
//                $regex = Regex::of($routerPattern, Regex::CASE_INSENSITIVE);
//
//                $string = $regex->with($url)->replaceWithCallback(function(Regex $self) {
//                    var_dump($self);
//                    var_dump($self->group(1));
//                    var_dump($self->group(2));
//                    return $self->group() * 2;
//                });
//                echo "CHECK: ";
//                var_dump($string);
//
//            }
//
//            matchUrl($url[1], '/([a-z]+)/([^/]+).js');


            Route::get('Profile', '/user/id[id]', function ($id) {
                echo "userpage: {$id}";
            });

            Route::get('Home', '/home/id[id]', 'App\Http\Controllers\IndexController@index');

//            $route = Route::getInstance();

//            $server = new WebServer([$route, 'dispatch']);

            $server = new WebServer(function (\php\webserver\WebRequest $request, \php\webserver\WebResponse $response) /*use(&$route)*/ {
//                Route::overrideInstance($route);

                Route::dispatch($request, $response);

//                echo "<h1>" . ($i++) . "<br></h1>\r\n";
//                echo $a;
//                $a = 'CHANGED OVER REQUEST!';
//                print_r($req);
//                echo "Hello World demo appp!";
////                if($req->servletPath != '/')
////                    $res->redirect('/');
//
////                $c = \php\io\Stream::getContents('tmp.php');
////                echo $c;
////                $b = String::replace($c, '<?php', '');
////                eval($b);
////                include "file://tmp.php";
////                include_once "file://tmp.php";
////                require "file://tmp.php";
////                require_once "file://tmp.php";
            });

            $server->setIsolated(false);

            $server->port = 3333;
            $server->isolated = false;
            $server->hotReload = false;
            $server->run();
        }
    }