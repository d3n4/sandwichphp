<?php

    namespace Sandwich\Provider;

    abstract class ServiceProvider
    {
        protected static $_registry = [];
        protected static $_singletons = [];

        /**
         * Call a service builder
         *
         * @param \Closure $builder builder closure
         * @param mixed $value
         * @param mixed $self
         * @return mixed
         */
        protected static function build($builder, $value, $self)
        {
            return $builder($value, $self);
        }

        /**
         * Dispose Service provider and all services instantiated
         */
        public static function dispose()
        {

        }

        /**
         * Instantiate service provider
         *
         * @param string $type
         * @param mixed $value Input data value
         * @param mixed $self Bounded object
         * @return mixed
         */
        public static function instance($type, $value = null, $self = null)
        {
            if (isset(static::$_registry[$type])) {
                return static::build(static::$_registry[$type], $value, $self);
            }

            return null;
        }

        /**
         * Bind virtual constructor to service provider class for further instantiate
         *
         * @param string $type Service provider class
         * @param callable $constructor Virtual constructor
         */
        public static function bind($type, callable $constructor)
        {
            static::$_registry[$type] = function ($value, $self) use ($constructor) {
                return $constructor($value, $self);
            };
        }

        /**
         * Bind virtual constructor to service provider class for further instantiate
         *
         * @param string $type Service provider class
         * @param callable $constructor Virtual singleton constructor
         */
        public static function singleton($type, callable $constructor)
        {
            static::$_registry[$type] = function ($value, $self) use ($type, $constructor) {
                if (!isset(static::$_singletons[$type])) {
                    static::$_singletons[$type] = $constructor($value, $self);
                }

                return static::$_singletons[$type];
            };
        }
    }