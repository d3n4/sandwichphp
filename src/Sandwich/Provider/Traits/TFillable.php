<?php

    namespace Sandwich\Provider\Traits;

    trait TFillable
    {
        public function fill($keyValues)
        {
            foreach ($keyValues as $key => $value) {
                if (isset($this->fillable)) {
                    if (in_array($key, $this->fillable)) {
                        $this->{$key} = $value;
                    }

                    continue;
                }

                $this->{$key} = $value;
            }

            return $this;
        }
    }