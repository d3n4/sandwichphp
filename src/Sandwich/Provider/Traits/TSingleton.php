<?php

    namespace Sandwich\Provider\Traits;

    trait TSingleton
    {

        /**
         * @var static
         */
        protected static $_singleton_instance;

        /**
         * @param callable $callback
         * @return static
         */
        public static function getInstance(callable $callback = null)
        {
            if (!static::$_singleton_instance) {
                static::$_singleton_instance = new static();
            }

            if(is_callable($callback))
                return $callback(static::$_singleton_instance);

            return static::$_singleton_instance;
        }

        public static function overrideInstance($instance) {
            return static::$_singleton_instance = $instance;
        }
    }