<?php

    namespace Sandwich\Provider\Traits;

    trait TProperties
    {
        public function __get($key) {
            $callee = [$this, 'get_' . $key];
            if(is_callable($callee))
                return call_user_func($callee);
            return null;
        }

        public function __set($key, $value) {
            $callee = [$this, 'set_' . $key];
            if(is_callable($callee))
                return call_user_func($callee, $value);
            return null;
        }
    }