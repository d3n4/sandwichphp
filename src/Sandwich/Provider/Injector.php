<?php

    namespace Sandwich\Provider;

    use Sandwich\Provider\TypeCast\ITypeCast;

    class Injector
    {

        const INJECT_DATA = 1;
        const INJECT_TYPECAST = 2;
        const INJECT_SERVICES = 4;
        const INJECT_OVERRIDES = 8;
        const INJECT_BINDS = 16;

        protected $_instance;
        protected $_reflection;
        protected $_data;
        protected $_overrides;
        protected $_types;

        public function __construct($data = null, &$instance = null)
        {
            if ($instance !== null) {
                $this->_instance =& $instance;
                $this->_reflection = new \ReflectionClass($this->_instance);
            }

            $this->_types = [];

            if ($data !== null) {
                $this->overrideData($data);
            }
        }

        /**
         * Override variable value by name (overrides without value processing)
         *
         * @param string $name
         * @param mixed $override
         * @return $this
         */
        public function override($name, $override = null)
        {
            if (is_array($name) && $override === null) {
                foreach ($name as $key => $value) {
                    $this->override($key, $value);
                }

                return $this;
            }

            $this->_overrides[strtolower(trim($name))] = $override;

            return $this;
        }

        /**
         * Override variable value by name (overrides with value processing)
         *
         * @param string|array $name
         * @param mixed $override
         * @return $this
         */
        public function overrideData($name, $override = null)
        {
            if (is_array($name) && $override === null) {
                foreach ($name as $key => $value) {
                    $this->overrideData($key, $value);
                }

                return $this;
            }

            $this->_data[strtolower(trim($name))] = $override;

            return $this;
        }

        /**
         * Bind type to instance
         *
         * @param $class
         * @param $instance
         * @return $this
         */
        public function Bind($class, &$instance)
        {
            $this->_types[$class] =& $instance;

            return $this;
        }

        /**
         * Trying to cast given argument type
         *
         * @param \ReflectionParameter $parameter
         * @param array $argument
         */
        protected function TypeCast(\ReflectionParameter $parameter, &$argument)
        {
            $type = $parameter->getClass();

            if (!$type) {
                return;
            }

            if (!$type->implementsInterface(ITypeCast::class)) {
                return;
            }

            if ($type->hasMethod('Cast') && $type->getMethod('Cast')->isStatic()) {
                $argument = $type->getMethod('Cast')->invoke(null, $argument, $this->_instance);
            }
        }

        /**
         * Binding input arguments data
         *
         * @param \ReflectionParameter $parameter
         * @param array $argument
         */
        protected function DataBind(\ReflectionParameter $parameter, &$argument)
        {
            if (isset($this->_data[strtolower($parameter->name)])) {
                $argument = $this->_data[strtolower($parameter->name)];
            }
        }

        /**
         * Binding input arguments data
         *
         * @param \ReflectionParameter $parameter
         * @param array $argument
         * @return boolean Binding result
         */
        protected function OverrideBind(\ReflectionParameter $parameter, &$argument)
        {
            if (isset($this->_overrides[strtolower($parameter->name)])) {
                $argument = $this->_overrides[strtolower($parameter->name)];

                return true;
            }

            return false;
        }

        /**
         * Binding input arguments data
         *
         * @param \ReflectionParameter $parameter
         * @param array $argument
         * @return boolean Binding result
         */
        protected function TypeBind(\ReflectionParameter $parameter, &$argument)
        {
            $type = $parameter->getClass();

            if (!$type) {
                return false;
            }

            if (isset($this->_types[$type->name])) {
                $argument = $this->_types[$type->name];

                return true;
            }

            return false;
        }

        /**
         * Injecting service providers
         *
         * @param \ReflectionParameter $parameter
         * @param array $argument
         */
        protected function ServiceProvider(\ReflectionParameter $parameter, &$argument)
        {
            $type = $parameter->getClass();

            if (!$type) {
                return;
            }

            $service = ServiceProvider::instance($type->name, $argument, $this->_instance);

            if ($service !== null) {
                $argument = $service;
            }
        }

        /**
         * Inject method and use $injectors as preprocessors
         *
         * @param string $method Injectable method name
         * @param int $injectors Injectors bitmask
         * @return null|callable Injected callback
         */
        public function inject($method, $injectors = null)
        {
            if ($injectors === null) {
                $injectors = self::INJECT_DATA | self::INJECT_TYPECAST | self::INJECT_SERVICES | self::INJECT_OVERRIDES | self::INJECT_BINDS;
            }

            $reflectionMethod = null;

            if ($this->_reflection) {
                $reflectionMethod = $this->_reflection->getMethod($method);
            } elseif (is_callable($method)) {
                $reflectionMethod = new \ReflectionFunction($method);
            }

            if (!$reflectionMethod) {
                return null;
            }

            $arguments = [];

            foreach ($reflectionMethod->getParameters() as $index => $parameter) {
                $arguments[$index] = null;

                if ($injectors & self::INJECT_OVERRIDES) {
                    if ($this->OverrideBind($parameter, $arguments[$index])) {
                        continue;
                    }
                }

                if ($injectors & self::INJECT_BINDS) {
                    if ($this->TypeBind($parameter, $arguments[$index])) {
                        continue;
                    }
                }

                if ($injectors & self::INJECT_DATA) {
                    $this->DataBind($parameter, $arguments[$index]);
                }

                if ($injectors & self::INJECT_TYPECAST) {
                    $this->TypeCast($parameter, $arguments[$index]);
                }

                if ($injectors & self::INJECT_SERVICES) {
                    $this->ServiceProvider($parameter, $arguments[$index]);
                }
            }

            return function () use ($reflectionMethod, $arguments) {
                if ($this->_reflection) {
                    return call_user_func_array($reflectionMethod->getClosure(is_object($this->_instance) ? $this->_instance : null), $arguments);
                } else {
                    return $reflectionMethod->invokeArgs($arguments);
                }
            };
        }
    }