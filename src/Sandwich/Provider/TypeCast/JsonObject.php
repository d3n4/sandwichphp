<?php

    namespace Sandwich\Provider\TypeCast;

    trait JsonObject
    {
        public static function Cast($value)
        {
            $object = null;

            try {
                $object = static::fromJSON($value);
            } catch (\Exception $e) {

            }

            return $object;
        }

        public static function fromJson($json)
        {
            $instance = new static();
            $json = json_decode($json, true);
            $class = new \ReflectionClass($instance);
            $properties = $class->getProperties(\ReflectionProperty::IS_PUBLIC);

            foreach ($properties as $property) {
                $name = $property->name;
                if (isset($json[$name])) {
                    $property->setValue($instance, $json[$name]);
                } else {
                    $property->setValue($instance, null);
                }
            }

            return $instance;
        }

        public function toJson()
        {
            $class = new \ReflectionClass($this);
            $properties = $class->getProperties(\ReflectionProperty::IS_PUBLIC);
            $prototype = [];

            foreach ($properties as $property) {
                $prototype[$property->name] = $property->getValue($this);
            }

            return json_encode($prototype);
        }
    }