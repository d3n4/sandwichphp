<?php

    namespace Sandwich\Provider\TypeCast;

    interface ITypeCast
    {
        public static function Cast($value);
    }