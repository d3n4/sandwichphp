<?php

    namespace Sandwich\Provider;

    interface IDisposable {
        public function dispose();
    }