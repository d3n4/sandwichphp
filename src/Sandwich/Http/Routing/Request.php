<?php

    namespace Sandwich\Http\Routing;

    use php\webserver\WebRequest;
    use php\webserver\WebResponse;
    use Sandwich\Http\Routing\Cookies\Cookies;
    use Sandwich\Provider\Traits\TProperties;

    /**
     * Class Request
     * @package Sandwich\Http\Routing
     * @var string $userAgent
     */
    class Request
    {
        use TProperties;

        /**
         * @var WebRequest
         */
        protected $request;

        /**
         * @var WebResponse
         */
        protected $response;

        /**
         * @var Cookies
         */
        protected $cookies;

        public function __construct(WebRequest $request, WebResponse $response)
        {
            $this->request = $request;
            $this->response = $response;
            $this->cookies = new Cookies($request->cookies, $response);
        }

        public function userAgent() {
            return $this->request->userAgent;
        }

        public function ip() {
            return $this->request->ip;
        }

        public function method() {
            return $this->request->method;
        }

        public function cookies() {
            return $this->cookies;
        }

        public function cookie($name) {
            return $this->cookies[$name];
        }
    }