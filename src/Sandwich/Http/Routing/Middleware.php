<?php

    namespace Sandwich\Http\Routing;

    use Sandwich\Http\Exceptions\InvalidHandlerException;
    use Sandwich\Http\Exceptions\MiddlewareListException;
    use Sandwich\Http\Exceptions\MiddlewareRegisteredException;
    use Sandwich\Provider\Injector;

    abstract class Middleware
    {
        protected static $_middleware = [];

        public static function register($name, callable $middleware)
        {
            if (isset(self::$_middleware[strtolower(trim($name))])) {
                throw new MiddlewareRegisteredException('Middleware "' . trim($name) . '" already registered');
            }
            if (is_callable($middleware)) {
                self::$_middleware[strtolower(trim($name))] = $middleware;
            } else {
                throw new InvalidHandlerException('Invalid handler was used');
            }
        }

        public static function run($middlewareList, callable $callback, callable $break = null, Request $request, Response $response)
        {
            if ($middlewareList === null) {
                if (is_callable($callback)) {
                    return call_user_func($callback);
                }
            }

            $stack = [];
            $stackArgs = [];
            if (!is_array($middlewareList)) {
                throw new MiddlewareListException('Middleware list must be an array but ' . gettype($middlewareList) . ' given');
            }
            foreach ($middlewareList as $middlewareName => $middlewareValue) {
                $middlewareArgs = null;
                if (is_array($middlewareValue)) {
                    $middlewareArgs = $middlewareValue;
                } elseif (is_string($middlewareValue)) {
                    $middlewareName = $middlewareValue;
                } else {
                    throw new MiddlewareListException('Middleware invalid type of an array item given, must be array or string but ' . gettype($middlewareValue) . ' given');
                }

                if ($middlewareArgs !== null) {
                    $stackArgs[$middlewareName] = $middlewareArgs;
                }

                $middlewareName = strtolower(trim($middlewareName));
                if (isset(self::$_middleware[$middlewareName])) {
                    $stack[$middlewareName] = self::$_middleware[$middlewareName];
                }
            }
            $chain = true;
            $next = function () use (&$chain) {
                $chain = true;
            };
            $currentMiddlewareName = null;
            $return = null;
            $result = null;
            foreach ($stack as $currentMiddlewareName => $callee) {
                $injector = new Injector();
                if (isset($stackArgs[$currentMiddlewareName])) {
                    $injector->override($stackArgs[$currentMiddlewareName]);
                }
                $injector->Bind(Request::class, $request);
                $injector->Bind(Response::class, $response);
                $injector->override('next', $next);
                $injected = $injector->inject($callee);
                if ($chain && !($chain = false)) {
                    $result = $injected();
                }
                if (!$chain) {
                    break;
                }
            }

            if ($chain) {
                if (is_callable($callback)) {
                    return call_user_func($callback);
                }
            } elseif ($break) {
                if (is_callable($break)) {
                    return call_user_func_array($break, [$currentMiddlewareName, $result]);
                }
            }

            return $return;
        }
    }