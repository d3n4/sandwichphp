<?php

    namespace Sandwich\Http\Routing\Cookies;

    use php\webserver\WebResponse;
    use Sandwich\Provider\Traits\TFillable;

    class Cookie
    {
        use TFillable;


        /**
         * @var WebResponse
         */
        protected $response;

        /**
         * @var string
         */
        public $name;

        /**
         * @var string
         */
        public $value;

        /**
         * @var string
         */
        public $path;

        /**
         * @var string
         */
        public $domain;

        /**
         * @var int
         */
        public $maxAge;

        /**
         * @var boolean
         */
        public $httpOnly;

        /**
         * @var boolean
         */
        public $secure;

        /**
         * @var string
         */
        public $comment;

        public function __construct($name = '', $value = '', $path = '/', $domain = '', $maxAge = -1, $httpOnly = false, $secure = false, $comment = '')
        {
            if (is_object($name)) {
                $this->response = $name;
                $name = '';
            }

            $this->name = $name;
            $this->value = $value;
            $this->path = $path;
            $this->domain = $domain;
            $this->maxAge = $maxAge;
            $this->httpOnly = $httpOnly;
            $this->secure = $secure;
            $this->comment = $comment;
        }

        public function save()
        {
            if ($this->response) {
                $this->response->addCookie($this->export());
            }
            return $this;
        }

        public function export()
        {
            return [
                'name'     => $this->name,
                'value'    => $this->value,
                'path'     => $this->path,
                'domain'   => $this->domain,
                'maxAge'   => $this->maxAge,
                'httpOnly' => $this->httpOnly,
                'secure'   => $this->secure,
                'comment'  => $this->comment,
            ];
        }
    }