<?php

    namespace Sandwich\Http\Routing\Cookies;

    use php\webserver\WebResponse;

    class Cookies implements \ArrayAccess
    {
        /**
         * @var Cookie[]
         */
        protected $cookies = [];
        protected $offset = 0;

        /**
         * @var WebResponse
         */
        protected $response;

        public function __construct($cookies, WebResponse $response)
        {
            $this->response = $response;

            if (is_array($cookies)) {
                foreach ($cookies as $raw_cookie) {
                    $cookie = new Cookie($response);
                    $cookie->fill($raw_cookie);
                    $this->cookies[$cookie->name] = $cookie;
                }
            }
        }

        /**
         * @param string $name
         * @param string $value
         * @param string $path
         * @param string $domain
         * @param int $maxAge
         * @param bool|false $httpOnly
         * @param bool|false $secure
         * @param string $comment
         * @return Cookie
         */
        public function create($name = '', $value = '', $path = '', $domain = '', $maxAge = -1, $httpOnly = false, $secure = false, $comment = '') {
            $cookie = new Cookie($this->response);

            $cookie->name = $name;
            $cookie->value = $value;
            $cookie->path = $path;
            $cookie->domain = $domain;
            $cookie->maxAge = $maxAge;
            $cookie->httpOnly = $httpOnly;
            $cookie->secure = $secure;
            $cookie->comment = $comment;

            return $cookie;
        }

        /**
         * @return Cookie[]
         */
        public function all() {
            return $this->cookies;
        }

        /**
         * @param string $offset
         * @return boolean
         */
        public function  offsetExists($offset)
        {
            return isset($this->cookies[$offset]);
        }

        /**
         * @param string $offset
         * @return Cookie|null
         */
        public function offsetGet($offset)
        {
            if (isset($this->cookies[$offset])) {
                return $this->cookies[$offset];
            }
            return null;
        }

        /**
         * @param string $offset
         * @param string $value
         * @return Cookie|null
         */
        public function  offsetSet($offset, $value)
        {
            if(!isset($this->cookies[$offset]))
                return null;
            return $this->cookies[$offset]->value = $value;
        }

        /**
         * @param string $offset
         */
        public function  offsetUnset($offset)
        {
            if (isset($this->cookies[$offset])) {
                $cookie = $this->cookies[$offset];
                unset($this->cookies[$cookie->name]);
            }
        }
    }