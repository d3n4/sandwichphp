<?php

    namespace Sandwich\Http\Routing;

    use php\lib\String;
    use php\util\Regex;
    use php\webserver\WebRequest;
    use php\webserver\WebResponse;
    use Sandwich\Http\Exceptions\RouteNotFoundException;
    use Sandwich\Provider\IDisposable;
    use Sandwich\Provider\Injector;
    use Sandwich\Provider\Traits\TSingleton;

    /**
     * Class Route
     * @package Sandwich\Http\Routing
     *
     * TODO: create dictionary for filtering static routes (adds performance for route searching)
     * TODO: make compatible with laravel router
     */
    class Route implements IDisposable
    {
        use TSingleton;

        /**
         * HTTP Methods
         */
        const METHOD_OPTIONS = 'OPTIONS', METHOD_GET = 'GET', METHOD_HEAD = 'HEAD', METHOD_POST = 'POST', METHOD_PUT = 'PUT', METHOD_DELETE = 'DELETE', METHOD_TRACE = 'TRACE', METHOD_CONNECT = 'CONNECT', METHOD_PATCH = 'PATCH';

        /**
         * Routes list include named routes (for indexing)
         * @var array
         */
        protected $routes = [];

        /**
         * Named routes list (for reverse route generator)
         * @var array
         */
        protected $namedRoutes = [];

        /**
         * Base url path
         * @var string
         */
        protected $basePath = '';

        /**
         * List of all types in route params
         * @var array
         */
        protected $matchTypes = [
            'i'  => '[0-9]++',
            'a'  => '[0-9A-Za-z]++',
            'h'  => '[0-9A-Fa-f]++',
            '*'  => '.+?',
            '**' => '.++',
            ''   => '[^/\.]++'
        ];

        /**
         * Dispatch route and execute action if found
         * if not throws RouteNotFound Exception
         *
         * @param \php\webserver\WebRequest $request
         * @param \php\webserver\WebResponse $response
         */
        public static function dispatch(WebRequest $request, WebResponse $response)
        {
            static::getInstance(function (Route $route) use ($request, $response) {
                $match = $route->findRoute($request->servletPath, $request->method);

                if ($match === null) {
                    throw new RouteNotFoundException($request->servletPath, $request->method);
                }

                list($handler, $params) = $match;

                $method = $handler['method'];
                $pattern = $handler['pattern'];
                $route = $handler['route'];
                $target = $handler['target'];
                $name = $handler['name'];
                $middleware = $handler['middleware'];

                $request = new Request($request, $response);
                $response = new Response($response);

                $response = Middleware::run($middleware, function () use ($target, $params, $request, $response) {
                    if (is_string($target)) {
                        list($controller, $action) = String::split($target, '@');

                        $controller = String::trim($controller);
                        $action = String::trim($action);

                        if (!class_exists($controller)) {
                            return null;
                        }

                        $methodRef = new \ReflectionMethod($controller, $action);

                        if ($methodRef->isStatic()) {
                            $injector = new Injector(null);
                        } else {
                            $instance = new $controller();
                            $injector = new Injector(null, $instance);
                        }

                        $injector->Bind(Request::class, $request);
                        $injector->Bind(Response::class, $response);
                        $injector->overrideData($params);

                        $injected = $injector->inject($action);

                        if ($injected) {
                            return $injected();
                        }

                        return null;
                    } elseif (is_callable($target)) {

                        $injector = new Injector();
                        $injector->Bind(Request::class, $request);
                        $injector->Bind(Response::class, $response);
                        $injector->overrideData($params);
                        $injected = $injector->inject($target);

                        if ($injected) {
                            return $injected();
                        }

                        return null;
                    }

                    return null;
                }, function ($chainName, $response) {
                    return $response;
                }, $request, $response);
            });
        }

        public static function get($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map(static::METHOD_GET, $uri, $action, $name, $middleware);
            });
        }

        public static function post($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map(static::METHOD_POST, $uri, $action, $name, $middleware);
            });
        }

        public static function put($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map(static::METHOD_PUT, $uri, $action, $name, $middleware);
            });
        }

        public static function patch($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map(static::METHOD_PATCH, $uri, $action, $name, $middleware);
            });
        }

        public static function delete($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map(static::METHOD_DELETE, $uri, $action, $name, $middleware);
            });
        }

        public static function options($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map(static::METHOD_OPTIONS, $uri, $action, $name, $middleware);
            });
        }

        public static function match($name, $methods, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $methods, $uri, $action, $middleware) {
                $route->map($methods, $uri, $action, $name, $middleware);
            });
        }

        public static function any($name, $uri, $action, $middleware = null)
        {
            static::getInstance(function (Route $route) use ($name, $uri, $action, $middleware) {
                $route->map([
                    static::METHOD_GET,
                    static::METHOD_POST,
                    static::METHOD_PUT,
                    static::METHOD_PATCH,
                    static::METHOD_DELETE,
                    static::METHOD_OPTIONS
                ], $uri, $action, $name, $middleware);
            });
        }

        public function __construct($routes = [], $basePath = '', $matchTypes = [])
        {
            $this->addRoutes($routes);
            $this->setBasePath($basePath);
            $this->addMatchTypes($matchTypes);
        }

        public function dispose()
        {
            $this->routes = [];
            $this->namedRoutes = [];
        }

        public function getRoutes()
        {
            return $this->routes;
        }

        public function addRoutes($routes)
        {
            if (!is_array($routes)) {
                throw new \Exception('Routes should be an array');
            }

            foreach ($routes as $route) {
                call_user_func_array([$this, 'map'], $route);
            }
        }

        public function setBasePath($basePath)
        {
            $this->basePath = $basePath;
        }

        public function addMatchTypes($matchTypes)
        {
            $this->matchTypes = array_merge($this->matchTypes, $matchTypes);
        }

        public function map($method, $route, $target, $name = null, $middleware = null)
        {
            if (!String::startsWith($route, '/')) {
                $route = '/' . $route;
            }

            if (String::endsWith($route, '/')) {
                $route = String::sub($route, 0, strlen($route) - 1);
            }

            if ($name) {
                $name = String::lower(String::trim($name));
            }

            $this->routes[] = [
                'method'     => is_array($method) ? $method : array_map(function ($e) {
                    return String::upper(String::trim($e));
                }, String::split($method, ',')),
                'pattern'    => $route,
                'route'      => $this->compileRoute($route),
                'target'     => $target,
                'name'       => $name,
                'middleware' => $middleware
            ];

            if ($name) {
                if (isset($this->namedRoutes[$name])) {
                    throw new \Exception("Can not redeclare route '{$name}'");
                } else {
                    $this->namedRoutes[$name] = $route;
                }
            }

            return;
        }

        public function generate($routeName, array $params = [])
        {
            throw new \Exception('Method is not implemented yet.');
        }

        public function findRoute($requestUrl = null, $requestMethod = null)
        {
            foreach ($this->routes as $handler) {
                if (!in_array($requestMethod, $handler['method'])) {
                    continue;
                }

                $regex = Regex::of($handler['route']['pattern'], Regex::CASE_INSENSITIVE)->with($requestUrl);

                $params = [];

                if ($regex->matches()) {
                    $paramsLength = sizeof($handler['route']['params']);
                    $groupsLength = $regex->getGroupCount();

                    if ($groupsLength > $paramsLength) {
                        throw new \Exception('Size of founded params is more than defined in route');
                    }

                    for ($paramIndex = 0; $paramIndex < $groupsLength; $paramIndex++) {
                        $param = $regex->group($paramIndex + 1);
                        $paramName = $handler['route']['params'][$paramIndex];
                        $params[$paramName] = $param;
                    }

                    return [$handler, $params];
                }
            }

            return null;
        }

        protected function compileRoute($route)
        {
            $params = [];
            $pattern = Regex::of('(\[(([a-z0-9\*_]+):)?([a-z_][a-z0-9]+)\])', Regex::CASE_INSENSITIVE)->with($route)->replaceWithCallback(function (Regex $self) use (&$params) {
                $patternId = $self->group(3);

                if ($patternId === null) {
                    $patternId = '';
                }

                $paramName = $self->group(4);
                $params[] = String::trim($paramName);

                return '(' . $this->matchTypes[$patternId] . ')';
            });

            return [
                'pattern' => $pattern,
                'params'  => $params
            ];
        }
    }