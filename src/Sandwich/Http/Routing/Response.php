<?php

    namespace Sandwich\Http\Routing;

    use php\webserver\WebResponse;
    use Sandwich\Http\Routing\Cookies\Cookie;

    class Response
    {
        protected $response;

        public function __construct(WebResponse $response)
        {
            $this->response = $response;
        }

        public function write($content) {
            $this->response->writeToBody($content);
            return $this;
        }

        public function type($type = 'text/html')
        {
            $this->response->contentType = $type;
            return $this;
        }

        public function status($status = 200)
        {
            $this->response->status = $status;
            return $this;
        }

        public function addCookie(Cookie $cookie)
        {
            $this->response->addCookie($cookie->export());
            return $this;
        }
    }