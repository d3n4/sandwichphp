<?php

    namespace Sandwich\Http\Exceptions;

    class RouteNotFoundException extends \Exception {
        public function __construct($url, $method) {
            parent::__construct("Route for url ({$method}) '{$url}' not found");
        }
    }