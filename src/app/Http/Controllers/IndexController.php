<?php

    namespace App\Http\Controllers;

    use php\time\Time;
    use Sandwich\Http\Controllers\Controller;
    use Sandwich\Http\Routing\Cookies\Cookie;
    use Sandwich\Http\Routing\Request;
    use Sandwich\Http\Routing\Response;

    class IndexController extends Controller
    {
        public function index($id, Request $request, Response $response)
        {
            $cookie = $request->cookies()->create('TEST_FROM_SERVER', Time::seconds())->save();

//            $request->cookies()['_ga']->value('HELLO WORLD :)' . Time::seconds())->save();

            $cookies = $request->cookies();
            print_r($cookies->all());

            foreach ($cookies->all() as $cook) {
                print_r($cook);
            }

            echo "1 cook:";
            print_r($cookies['_ga']);
//            print_r($request);
            echo "USER IP:" . $request->ip();
            echo "homepage: {$id}";
        }
    }